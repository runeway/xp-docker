#!/bin/bash

# Shared variables
export ROOT=/srv/app
export REPO_BASE=http://repo.enonic.com/public/com/enonic/wem
export DISTRO=$REPO_BASE/wem-distro/5.0.0-SNAPSHOT/wem-distro-5.0.0-SNAPSHOT.tar.gz
export TMP_DIR=$ROOT/tmp
export APP_DIR=$ROOT/wem-distro-5.0.0-SNAPSHOT

# Download files
rm -rf $TMP_DIR
mkdir $TMP_DIR
cd $TMP_DIR
wget $REPO_BASE/wem-distro/5.0.0-SNAPSHOT/wem-distro-5.0.0-SNAPSHOT.tar.gz

# Unpack distro
cd $ROOT
rm -rf $APP_DIR
tar xzf $TMP_DIR/wem-distro-5.0.0-SNAPSHOT.tar.gz

# Copy over karaf files
cp -R $ROOT/karaf/* $APP_DIR/.

# Get optional modules
cd $APP_DIR/deploy
wget $REPO_BASE/modules/xeon/1.0.0/xeon-1.0.0.jar
wget $REPO_BASE/modules/xslt/1.0.0/xslt-1.0.0.jar
wget $REPO_BASE/modules/demo/1.0.0/demo-1.0.0.jar
wget $REPO_BASE/modules/seedsavers/1.0.0/seedsavers-1.0.0.jar
wget $REPO_BASE/modules/features/1.0.0/features-1.0.0.jar

# Launch karaf
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
$APP_DIR/bin/karaf server
