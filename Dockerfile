# Base image
FROM ubuntu:14.04

# Set timezone to Europe/Oslo
ADD config/timezone /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

# Accecpt Oracle license before installing java
RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
RUN echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections

# Install java
RUN apt-get update
RUN apt-get -y install software-properties-common
RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get update
RUN apt-get -y install oracle-java8-jdk
RUN echo "export JAVA_HOME=/usr/lib/jvm/java-8-oracle" >> /etc/environment

# Install other software
RUN apt-get -y install git wget

# Exposing server port
EXPOSE 8080

# Copy some files
RUN mkdir /srv/app
ADD script/launcher.sh /srv/app/launcher.sh
RUN chmod +x /srv/app/launcher.sh
ADD karaf/ /srv/app/karaf

# Start the launcher
CMD /srv/app/launcher.sh
