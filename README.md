# Docker build for Enonic XP

This repository contains the sources for docker image with Enonic XP.
The product is currently in ALPHA stage and this docker image should be
not used in production.

## Building

To build this image, run the following command:

    docker build --rm --tag=xp-snapshot .

## Start the image

To run this image:

    docker run -d --name enonic-xp xp-snapshot

This is exposing port 8080. To map this port to 80 you can run using the port mapping:

    docker run -d --name enonic-wem -p 80:8080 xp-snapshot

